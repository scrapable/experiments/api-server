const fs = require('fs');
const defaultPath = './scrappers/';
const fileEnding = 'json';
const browser = require('viscrapable-browser-host');


(async () => {
	
	

function listScrapersFromDirectory(directoryPath)
{
console.log(`Looking for Files in ${directoryPath}`);
fs.readdir(directoryPath,listScrapers);
}

function listScrapers(err,files) {
	if (files===undefined||files===null) return;
	if(Error)console.log(`Error: ${err}`);
	console.log(files);
	const fileNameArrayJsons = files.filter(isJson);
	//const fileNameArrayNonJsons = files.filter(name=>name.contains);

	console.log(`Of which ${fileNameArrayJsons.length} are .${fileEnding} Files :`);
	fileNameArrayJsons.forEach(x => console.log(x));
}

async function executeScraperFromFilePath(filePath)
{
console.log('executescraperFromFilePath:'+filePath);
jsonReader(filePath,(err,scrapper)=>{
	if(err){
	console.log(err);
	return;
	}
	 executeScraper(scrapper);//await
})
}

async function executeScraper(scraper)
{
	await browser.launch({headless:false});	
	//scraper.forEach(o=> executeOperation(o));

	const start = async () => {
		await asyncForEach(scraper, async (o) => {
		  await browser.execute(o);		  
		});
	  };
	start();

	// use normal for loop
	//await browser.execute({op:'goto',args:{url:'https://www.google.com/'}});
//	await browser.screenshot().then(base64=>saveBase64(base64,"screenshot.jpg"));
	await browser.close();

}

async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
	  await callback(array[index], index, array);
	}
  }

async function executeOperation(operation)
{
	await browser.execute(operation);
}



async function main()
{
	
//	executescraperFromFilePath(' ');
// eslint-disable-next-line prefer-destructuring
const pathArgument = process.argv[2];
let path = defaultPath;
console.log(pathArgument);
if (pathArgument!==undefined && pathArgument !== null) {
	path=pathArgument;
}
console.log(`Path :${path}`);
if (path.endsWith(fileEnding))
{
executeScraperFromFilePath(path);
}
else
{
listScrapersFromDirectory(path);
}
}



function isJson(name) {
	if(!name)return false;
	return name.endsWith(fileEnding);
}

async function jsonReader(filePath, cb) {
    fs.readFile(filePath, (err, fileData) => {
        if (err) {
            return cb && cb(err)
        }
        try {
            const object = JSON.parse(fileData)
            return cb && cb(null, object)
        } catch(err) {
            return cb && cb(err)
        }
    })
}

async function saveBase64(dataURI,filename) {
	console.log(`saveBase64:file: [${filename}]`);
	console.log(dataURI);
	// convert base64 to raw binary data held in a string
	// doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this


	var byteString = atob(dataURI.split(',')[1]);
  
	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
  
	// write the bytes of the string to an ArrayBuffer
	var ab = new ArrayBuffer(byteString.length);
  
	// create a view into the buffer
	var ia = new Uint8Array(ab);
  
	// set the bytes of the buffer to the correct values
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
  
	// write the ArrayBuffer to a blob, and you're done
	var blob = new Blob([ab], {type: mimeString});
 
	var FileSaver = require('file-saver');
    //var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
	FileSaver.saveAs(blob, filename);
	
  
  }

async function saveBase64Smaller(dataURI,filename)
{
	var url = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
	var FileSaver = require('file-saver');
fetch(url)
.then(res => res.blob())
.then(blob => FileSaver.saveAs(blob, filename));

	
}

main();
})();


// diplomarbeit-db meilensteine auf montag
// nodejs express server
// kanban-board